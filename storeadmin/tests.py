from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.conf import settings
from importlib import import_module
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session

from importlib import import_module
from django.conf import settings as django_settings

from storeadmin.models import *
from landingpage.models import *
import storeadmin.views 

"""
class SessionTestCase(TestCase):
    def get_session(self):
        if self.client.session:
            session = self.client.session
        else:
            engine = import_module(django_settings.SESSION_ENGINE)
            session = engine.SessionStore()
        return session
    
    def set_session_cookies(self, session):
        # Set the cookie to represent the session
        session_cookie = django_settings.SESSION_COOKIE_NAME
        self.client.cookies[session_cookie] = session.session_key
        cookie_data = {
            'max-age': None,
            'path': '/',
            'domain': django_settings.SESSION_COOKIE_DOMAIN,
            'secure': django_settings.SESSION_COOKIE_SECURE or None,
            'expires': None}
        self.client.cookies[session_cookie].update(cookie_data)

"""
class StoreAdminUnitTest(TestCase) :

    def test_storeadmin_url_is_exist_not_logged_in (self) :
        response = Client().get('/admin/login')
        self.assertEqual(response.status_code, 200)

        response = Client().get('/admin/')
        self.assertEqual(response.status_code, 302)
        
        response = Client().get('/admin/manage/add')
        self.assertEqual(response.status_code, 302)

        response = Client().get('/admin/manage')
        self.assertEqual(response.status_code, 302)

    def test_storeadmin_url_is_exist_logged_in (self) :
        user = User.objects.create_user("admin", "admin@admin.com", "admin")

        logged_in = self.client.force_login(user=user)
        response = self.client.get("/admin/login")
        self.assertEqual(response.status_code, 302)
        
        response = self.client.get('/admin/manage/add')
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/admin/manage')
        self.assertEqual(response.status_code, 200)

    def test_storeadmin_login_post (self) :
        user = User.objects.create_user("admin", "admin@admin.com", "admin")
        response = Client().post("/admin/login", data={
            "username" : "admin",
            "password" : "admin"
        })
        self.assertURLEqual(response.url, "/admin/manage")

    def test_storeadmin_wrong_login_post (self) :
        user = User.objects.create_user("admin", "admin@admin.com", "admin")
        response = Client().post("/admin/login", data={
            "username" : "admin",
            "password" : "salahin"
        })
        html_response = response.content.decode('utf8')
        self.assertIn("Invalid Username or Password", html_response)
        
    def test_storeadmin_model_create (self) :
        Kategori.objects.create(nama="test_kategori")
        kategori = Kategori.objects.get(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )
        user = User.objects.create_user("admin", "admin@admin.com", "admin")
        logged_in = self.client.force_login(user=user)

        response = self.client.get("/admin/")
        html_response = response.content.decode('utf8')
        self.assertIn("barang_test", html_response)

    def test_storeadmin_model_delete (self) :
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )
        user = User.objects.create_user("admin", "admin@admin.com", "admin")
        logged_in = self.client.force_login(user=user)

        response = self.client.get("/admin/manage/delete/1")
        count = Barang.objects.all().count()
        self.assertEqual(count, 0)

    def test_storeadmin_model_update (self) :
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )
        user = User.objects.create_user("admin", "admin@admin.com", "admin")
        logged_in = self.client.force_login(user=user)

        response = self.client.post("/admin/manage/update/1", data={
            "nama" : "nama_baru",
            "stok" : 3,
            "harga" : 123,
            "deskripsi" : "deskripsi_baru",
            "kategori" : kategori,
        })
        response = self.client.get("/admin/manage")
        html_response = response.content.decode('utf8')
        self.assertIn("nama_baru", html_response)

    def test_storeadmin_model_create_post (self) :
        user = User.objects.create_user("admin", "admin@admin.com", "admin")
        logged_in = self.client.force_login(user=user)

        kategori = Kategori.objects.create(nama="test_kategori")
        response = self.client.post("/admin/manage/add", data={
            "nama" : "barang_baru",
            "harga" : 123,
            "stok" : 3,
            "deskripsi" : "deskripsi_baru",
            "kategori" : kategori.id,
        })
        count = Barang.objects.all().count()
        self.assertEqual(count, 1)
"""
    def test_storeadmin_template_used (self) :
        response = Client.get('/login')
        self.assertTemplateUsed(response, 'admin_login.html')

        session = self.session
        session['LOGGED_IN'] = True
        session.save()
        response = Client().get('/')
        self.assertTemplateUsed(response, 'admin_manage.html')


    def test_storeadmin_view_show (self) :
        Kategori.create(nama="test_kategori")
        kategori = Kategori.get(nama="test_kategori")

        barang = Barang.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )

        request = HttpRequest()
        response = storeadmin.views.login(request)

        html_response = response.content.decode('utf8')
        self.assertIn('')

    def test_storeadmin_using_index (self) :
        found = resolve('/')
        self.assertEqual(found.func, storeadmin.views.manage)

    def test_storeadmin_ (self) :
        pass

    def test_storeadmin_ (self) :
        pass

    def test_storeadmin_ (self) :
        pass

    def test_storeadmin_ (self) :
        pass

    def test_storeadmin_ (self) :
        pass

    def test_storeadmin_ (self) :
        pass

    def test_storeadmin_ (self) :
        pass

"""