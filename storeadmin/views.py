from django.shortcuts import render, redirect
from django.urls import reverse
from .models import Barang
from .forms import *
from landingpage.forms import KategoriForm
from django.contrib import messages
from django.contrib.auth import authenticate, login as django_login
from django.contrib.auth.decorators import login_required

def login (request) :
    if(request.user.is_authenticated) :
        return redirect("/admin/manage")

    if(request.method == "POST") :
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(username=username, password=password)
        
        if(user is not None) :
            django_login(request, user)
            return redirect("/admin/manage")
        
        else :
            messages.error(request, "Invalid Username or Password")

    return render(request, 'admin_login.html')

@login_required(login_url="/admin/login")
def manage (request) :
    products = Barang.objects.all()
    product_list = []
    for product in products :
        product_list.append((product.id, BarangEditForm(instance=product), product.dbx_link))

    form_kategori = KategoriForm()
    form = BarangEditForm()

    context = {
        "products" : product_list,
        "form" : form,
        "category_form" : form_kategori,
    }

    return render(request, 'admin_manage.html', context)

@login_required(login_url="/admin/login")
def delete (request, id) :
    data = Barang.objects.get(id=id)
    data.delete()

    messages.success(request, 'Barang berhasil dihapus')
    return redirect(reverse("storeadmin:manage"))

@login_required(login_url="/admin/login")
def update (request, id) :
    old_data = Barang.objects.get(id=id)
    old_data.nama = request.POST.get('nama')
    old_data.stok = request.POST.get('stok')
    old_data.harga = request.POST.get('harga')
    old_data.deskripsi = request.POST.get('deskripsi')
    old_data.save()

    messages.success(request, 'Data barang berhasil diubah.')
    return redirect(reverse("storeadmin:manage"))

@login_required(login_url="/admin/login")
def add (request) :
    data = BarangEditForm(request.POST, request.FILES)

    if(data.is_valid()) :
        data.save()
    
    else :
        print(data.errors)

    messages.success(request, 'Barang berhasil ditambahkan.')
    return redirect(reverse("storeadmin:manage"))
