from django.db import models
from landingpage.models import *
from django.core.validators import *
from dropbox.dropbox import Dropbox

from django.db.models.signals import post_delete
from django.dispatch import receiver

class Barang(models.Model):
    nama = models.CharField(max_length = 64)
    harga = models.IntegerField(validators=[MinValueValidator(0)])
    stok = models.IntegerField(validators=[MinValueValidator(0)])
    deskripsi = models.TextField()
    foto = models.ImageField(upload_to="", blank=True)
    kategori = models.ForeignKey(Kategori, null=True, on_delete=models.SET_NULL)
    
    def __str__(self):
        return self.nama

    def dbx_link (self) :
        try:
            dbx = Dropbox("omMNqUb0sSAAAAAAAAAADt4I9h-3pLeZLUFDag1X2ng5gxC9y7z-PnCbHMkciTCA")
            return dbx.sharing_create_shared_link(f"/{self.foto}").url[:-4] + "raw=1"
        except:
            return ""

@receiver(post_delete, sender=Barang)
def submission_delete(sender, instance, **kwargs) :
    instance.foto.delete(False) 
