from django.urls import path

from . import views

app_name = 'storeadmin'

urlpatterns = [
    path('', views.manage, name='manage'),
    path('manage/add', views.add, name='add'),
    path('manage/delete/<int:id>', views.delete, name='delete'),
    path('manage', views.manage, name='manage'),
    path('manage/update/<int:id>', views.update, name='update'),
    path('login', views.login, name='login'),
]