from django.db import models
from django.core.validators import *

from storeadmin.models import Barang
from kupon.models import Kupon

# Create your models here.
class Transaksi(models.Model):
    nama_pembeli = models.CharField(max_length=64)
    barang = models.ForeignKey(Barang, on_delete=models.CASCADE, default=0)
    kupon = models.ForeignKey(Kupon, on_delete=models.CASCADE, null=True)
    total_transaksi = models.IntegerField(validators=[MinValueValidator(0)])
    tanggal_beli = models.DateTimeField(auto_now=True)
