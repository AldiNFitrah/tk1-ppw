from django.shortcuts import render, redirect
from storeadmin.models import Barang
from kupon.models import Kupon
from checkout.models import Transaksi
import datetime

# Create your views here.
def index(request):
    if 'barang_id' in request.POST:
        try:
            barang_detail = Barang.objects.get(id=request.POST['barang_id'])
        except:
            return redirect('/')

        if barang_detail.stok >0:
            kupon = Kupon.objects.filter(tanggal_expired_kupon__gte = datetime.datetime.now(), harga_minimum__lte = barang_detail.harga)
            barang_detail.harga =  f'{barang_detail.harga:,}'.replace(',','.')
            return render(request, 'checkout.html', {'barang':barang_detail, 'kupon':kupon})
        else:
            return redirect('/')

    else:
        return redirect('/')

def done(request):
    if 'barang_id' in request.POST and 'nama' in request.POST and 'phone-number' in request.POST and 'payment-method' in request.POST and 'coupon-id' in request.POST:
        data = request.POST
        barang = Barang.objects.get(id=data['barang_id'])
        totaltransaksi = barang.harga
        totaldiskon = 0
        if data['coupon-id'] != '':
            kupon = Kupon.objects.get(id=data['coupon-id'])
            totaldiskon = kupon.persen_diskon*totaltransaksi/100
            totaltransaksi -= totaldiskon
            transaksi = Transaksi(nama_pembeli = data['nama'], barang = barang, kupon = kupon, total_transaksi = totaltransaksi )
        else:
            totaldiskon = 0
            transaksi = Transaksi(nama_pembeli = data['nama'], barang = barang, total_transaksi = totaltransaksi )
            kupon = ''
        
        transaksi.save()
        barang.stok = barang.stok-1
        barang.save()

        barang.harga =  f'{barang.harga:,}'.replace(',','.')
        totaltransaksi = f'{totaltransaksi:,}'.replace(',','.')

        return render(request, 'checkoutdone.html',{'barang':barang, 'kupon':kupon, 'totaltransaksi':totaltransaksi, 'nama' : data['nama'], 'phonenumber' : data['phone-number'], 'paymentmethod' : data['payment-method'], 'totaldiskon':totaldiskon})
    else:
        return redirect('/')