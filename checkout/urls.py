from django.urls import path, include
from . import views

urlpatterns = [
    path('',views.index, name='checkoutpage'),
    path('complete', views.done, name="checkoutcomplete")
]
