from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
import datetime

import checkout.models
import storeadmin.models
from kupon.models import Kupon
import landingpage.models
from .views import index

class CheckoutTest(TestCase) :
    
    def test_checkout_is_exist(self) :
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_checkout_model_create(self) :
        kategori = landingpage.models.Kategori.objects.create(nama="kategori_test")
        barang = storeadmin.models.Barang.objects.create(
            nama="barang_test",
            harga=1234,
            stok=1234,
            deskripsi="test_test",
            kategori=kategori,
        )
        kupon = Kupon.objects.create(
            nama_kupon="kupon_test",
            jenis_kupon="kupon_test",
            sk_kupon="apa",
            sk_singkat_kupon = "apa",
            kode="kode_test",
            persen_diskon=50,
            harga_minimum=1000,
            tanggal_expired_kupon=datetime.datetime.now()+datetime.timedelta(days=1)
        )
        
        checkout.models.Transaksi.objects.create(
            nama_pembeli="dummy",
            barang=barang,
            kupon=kupon,
            total_transaksi=10000,
            tanggal_beli=datetime.datetime.now()
        )

        jumlah = checkout.models.Transaksi.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_checkout_no_data_redirect(self) :
        response = Client().get('/checkout/complete')
        self.assertEqual(response.status_code,302)

    def test_checkout_get_redirect(self):
        response = Client().get('/checkout/')
        self.assertEqual(response.status_code, 302)

    def test_checkout_post_nodata(self):
        data = {}
        response = Client().post('/checkout/', data=data)
        self.assertEqual(response.status_code,302)

    def test_checkout_post_not_found(self):
        data = {
            'barang_id' :1
        }
        response = Client().post('/checkout/', data=data)
        self.assertEqual(response.status_code, 302)

    def test_checkout_post_item_found(self):
        kategori = landingpage.models.Kategori.objects.create(nama="kategori_test")
        barang = storeadmin.models.Barang.objects.create(
            nama="barang_test",
            harga=1234,
            stok=1234,
            deskripsi="test_test",
            kategori=kategori,
        )
        data = {
            'barang_id':barang.id
        }
        response = Client().post('/checkout/', data=data)
        self.assertEqual(response.status_code,200)
    def test_checkout_post_item_zero_stock(self):
        kategori = landingpage.models.Kategori.objects.create(nama="kategori_test")
        barang = storeadmin.models.Barang.objects.create(
            nama="barang_test",
            harga=1234,
            stok=0,
            deskripsi="test_test",
            kategori=kategori,
        )
        data = {
            'barang_id':barang.id
        }
        response = Client().post('/checkout/', data=data)
        self.assertEqual(response.status_code,302)

    def test_checkout_post_done_no_coupon(self):
        kategori = landingpage.models.Kategori.objects.create(nama="kategori_test")
        barang = storeadmin.models.Barang.objects.create(
            nama="barang_test",
            harga=1234,
            stok=1234,
            deskripsi="test_test",
            kategori=kategori,
        )
        data = {
            'barang_id':barang.id,
            'nama' : 'aaa',
            'phone-number' : '000',
            'payment-method' : 'bank-transfer',
            'coupon-id' : '',
        }
        response = Client().post('/checkout/complete', data=data)
        self.assertEqual(response.status_code,200)

    def test_checkout_post_done_coupon(self):
        kategori = landingpage.models.Kategori.objects.create(nama="kategori_test")
        barang = storeadmin.models.Barang.objects.create(
            nama="barang_test",
            harga=1234,
            stok=1234,
            deskripsi="test_test",
            kategori=kategori,
        )

        kupon = Kupon.objects.create(
            nama_kupon="kupon_test",
            jenis_kupon="kupon_test",
            sk_kupon="apa",
            sk_singkat_kupon = "apa",
            kode="kode_test",
            persen_diskon=50,
            harga_minimum=1000,
            tanggal_expired_kupon=datetime.datetime.now()+datetime.timedelta(days=1)
        )
        data = {
            'barang_id':barang.id,
            'nama' : 'aaa',
            'phone-number' : '000',
            'payment-method' : 'bank-transfer',
            'coupon-id' : kupon.id,
        }
        response = Client().post('/checkout/complete', data=data)
        self.assertEqual(response.status_code,200)

