from django.shortcuts import render

from storeadmin.models import Barang
from landingpage.models import Kategori

category_link = {
    'stationary-set' : 'Stationary',
    'books' : 'Books',
    'eraser' : 'Eraser',
    'pencil-case' : 'Pencil Case',
    'tools' : 'Tools',
    'tape' : 'Tape'
    }

def index(request):
    kumpulan_filtered = []
    data_all = Barang.objects.all()
    category_filter = False
    search_filter = False
    nama_kategori = ''
    q = ''
    
    if 'category' in request.GET and request.GET['category'] in category_link:
        nama_kategori = category_link[request.GET['category']]
        kategori = Kategori.objects.filter(nama=nama_kategori)
        if kategori.count() >0 :
            kategori = kategori[0]
            kumpulan_filtered.append(Barang.objects.filter(kategori=kategori))
        else:
            nama_kategori = 'Category Not Found'
        category_filter = True

    if 'q' in request.GET:
        kumpulan_filtered.append(Barang.objects.filter(nama__contains=request.GET['q']))
        search_filter = True
        q = request.GET['q']

    data_serve = []
    for data in data_all:
        fit_to_filter = True
        for dataset in kumpulan_filtered:
            if data not in dataset:
                fit_to_filter = False
        if fit_to_filter:
            data_serve.append(data)
    
    for x in range(len(data_serve)):
        data_serve[x].harga =  f'{data_serve[x].harga:,}'.replace(',','.')
        
    if category_filter or search_filter :
        return render(request, 'indexsearch.html', {'barang': data_serve, 'category_filter' : category_filter, 'category':nama_kategori, 'search_filter' : search_filter, 'q':q})
    else:
        return render(request, 'index.html', {'barang':data_serve})