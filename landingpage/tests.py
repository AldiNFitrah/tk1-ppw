from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

from storeadmin.models import Barang
from landingpage.models import Kategori
from detailpage.models import Ulasan
import detailpage.views

from django.conf import settings

class LandingPageUnitTest(TestCase) :
    def test_landing_page_status_ok(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_landing_page_category_filter(self):
        kategori = Kategori.objects.create(
            nama = 'Tools'
        )
        data = {
            'category': 'tools'
        }
        response = Client().get('/', data=data)
        self.assertEqual(response.status_code, 200)

    def test_landing_page_category_not_found(self):
        data = {
            'category': 'tools'
        }
        response = Client().get('/', data=data)
        self.assertEqual(response.status_code, 200)

    def test_landing_page_with_search(self):
        data = {
            'q':'Pensil'
        }
        response = Client().get('/', data=data)
        self.assertEqual(response.status_code,200)

    def test_landing_page_with_barang(self):
        kategori = Kategori.objects.create(nama="Tools")
        barang = Barang.objects.create(
            nama="barang_test",
            harga=1234,
            stok=1234,
            deskripsi="test_test",
            kategori=kategori,
        )
        barang = Barang.objects.create(
            nama="barang_2",
            harga=1234,
            stok=1234,
            deskripsi="test_test",
            kategori=kategori,
        )
        data = {
            'category':'tools'
        }
        response = Client().get('/', data=data)
        self.assertEqual(response.status_code,200)

    def test_landing_page_test_search_result(self):
        kategori = Kategori.objects.create(nama="Tools")
        barang = Barang.objects.create(
            nama="barang_test",
            harga=1234,
            stok=1234,
            deskripsi="test_test",
            kategori=kategori,
        )
        barang = Barang.objects.create(
            nama="barang_2",
            harga=1234,
            stok=1234,
            deskripsi="test_test",
            kategori=kategori,
        )
        data = {
            'q':'barang'
        }
        response1 = Client().get('/', data=data)
        data = {
            'q':'barang_test'
        }
        response2 = Client().get('/', data=data)
        self.assertLess(response2.content, response1.content)

