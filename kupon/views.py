from django.shortcuts import render
from .models import Kupon
import datetime

def index(request):
    kupon = Kupon.objects.filter(tanggal_expired_kupon__gte = datetime.datetime.now())
    return render(request, "kupon.html", {'kupon':kupon})
