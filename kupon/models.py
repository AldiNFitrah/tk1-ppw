from django.db import models
from django.core.validators import *
from dropbox.dropbox import Dropbox


class Kupon(models.Model):
    nama_kupon = models.CharField(max_length=64)
    gambar = models.ImageField(upload_to="", blank=True)
    jenis_kupon = models.CharField(max_length=128)
    sk_singkat_kupon = models.CharField(max_length=256)
    sk_kupon = models.TextField()
    kode = models.CharField(max_length = 64)
    persen_diskon = models.IntegerField(validators=[MinValueValidator(0),MaxValueValidator(100)])
    harga_minimum = models.IntegerField(validators=[MinValueValidator(0)])
    tanggal_expired_kupon = models.DateTimeField(auto_now=False)

    def __str__(self):
        return self.jenis_kupon
    
    def dbx_link (self) :
        try:
            dbx = Dropbox("omMNqUb0sSAAAAAAAAAADt4I9h-3pLeZLUFDag1X2ng5gxC9y7z-PnCbHMkciTCA")
            return dbx.sharing_create_shared_link(f"/{self.gambar}").url[:-4] + "raw=1"
        except:
            return ""