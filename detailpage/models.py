from django.db import models
from storeadmin.models import Barang


class Ulasan(models.Model):
    nama_pengulas = models.CharField(max_length=15, blank=False)
    isi_ulasan = models.TextField(max_length=130, blank=False)
    penilaian = models.IntegerField(default=1)
    barang = models.ForeignKey(Barang, on_delete=models.CASCADE, default=0)
    tanggal_dibuat = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"[{self.barang}] - {self.nama_pengulas} - ({self.penilaian})"
