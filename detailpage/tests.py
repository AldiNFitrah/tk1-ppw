from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

from storeadmin.models import *
from landingpage.models import *
from detailpage.models import *
import detailpage.views

from django.conf import settings

class DetailPageUnitTest(TestCase) :
    def test_detailpage_url_is_exist (self) :
        Kategori.objects.create(nama="test_kategori")
        kategori = Kategori.objects.get(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )

        response = Client().get('/detail/')
        self.assertEqual(response.status_code, 302)
        
        response = Client().get('/detail/1')
        self.assertEqual(response.status_code, 200)

    def test_detailpage_model_create (self) :
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )

        jumlah = Barang.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_detailpage_template_used (self) :
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )

        response = Client().get('/detail/1')
        self.assertTemplateUsed(response, 'detailpage.html')


    def test_detailpage_view_show (self) :
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )

        request = HttpRequest()
        response = detailpage.views.index(request, 1)
        html_response = response.content.decode('utf8')
        self.assertIn('barang_test', html_response)

    def test_detailpage_view_show_ulasan (self) :
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )

        ulasan = Ulasan.objects.create(
            nama_pengulas="dummy",
            isi_ulasan="bagus",
            penilaian="4",
            barang=barang
        )
        request = HttpRequest()
        response = detailpage.views.index(request, 1)
        html_response = response.content.decode('utf8')
        self.assertIn('bagus', html_response)

    def test_detailpage_using_no_id (self) :
        found = resolve('/detail/')
        self.assertEqual(found.func, detailpage.views.no_id)

    def test_detailpage_using_index (self) :
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )

        found = resolve('/detail/1')
        self.assertEqual(found.func, detailpage.views.index)

    def test_detailpage_save_a_POST_request(self):
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )
        
        response = Client().post('/detail/1', data={
            'nama':"dummy",
            'isi':"bagus",
            'rating':'1',
        })

        jumlah = Ulasan.objects.filter(barang=barang).count()
        self.assertEqual(jumlah,1)
        self.assertEqual(response.status_code, 200)

    def test_detailpage_model_name(self):
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )

        ulasan = Ulasan.objects.create(
            nama_pengulas="dummy",
            isi_ulasan="bagus",
            penilaian="4",
            barang=barang
        )

        ulasan = Ulasan.objects.get(nama_pengulas="dummy")
        self.assertEqual(str(ulasan),"[barang_test] - dummy - (4)")

    def test_detailpage_barang_not_found_redirect(self):
        response = Client().get('/detail/0')
        self.assertEqual(response.status_code, 302)
        
