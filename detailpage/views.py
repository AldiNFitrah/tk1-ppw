from django.shortcuts import render, redirect
from storeadmin.models import Barang
from checkout.models import Transaksi
from detailpage.models import Ulasan
from django.views.generic import CreateView


def index(request, pk):
    if request.method == 'POST':
        barang = Barang.objects.get(id=pk)
        nama = request.POST['nama']
        isi = request.POST['isi']
        nilai = request.POST['rating']
        ulasanObj = Ulasan(nama_pengulas=nama,
                           isi_ulasan=isi, barang=barang, penilaian=nilai)
        ulasanObj.save()  
    barang = Barang.objects.filter(id=pk)
    if barang.count() == 0:
        return redirect('/')
    barang = barang[0]
    count = Transaksi.objects.filter(barang = barang).count()
    ulasan = Ulasan.objects.filter(barang = barang)
    ulasan_score = 0
    if ulasan.count() != 0:
        for x in ulasan:
            ulasan_score += x.penilaian
        ulasan_score /= ulasan.count()
    else:
        ulasan_count = 0
    ulasan_score = round(ulasan_score,1)
    ulasan_star = range(round(ulasan_score))
    ulasan_no_star = range(5-round(ulasan_score))
    jumlah_ulasan = ulasan.count()
    ulasan_with_star = []

    # Format Price
    barang.harga = f'{barang.harga:,}'.replace(',','.')
    
    for x in ulasan:
        ulasan_with_star.append((x,range(x.penilaian), range(5-x.penilaian)))

    context = {
        'Ulasan': ulasan_with_star,
        'Barang': barang,
        'Sold' : count,
        'Star' : ulasan_star,
        'NoStar' : ulasan_no_star,
        'UlasanScore' : ulasan_score,
        'UlasanCount' : jumlah_ulasan
    }
    return render(request, 'detailpage.html', context)

def no_id(request):
    return redirect('/')