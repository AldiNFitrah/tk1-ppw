from django.apps import AppConfig


class HistorypageConfig(AppConfig):
    name = 'historypage'
