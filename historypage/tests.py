from django.test import TestCase
from django.test import Client
import datetime

from checkout.models import *
from storeadmin.models import *
from kupon.models import *
from landingpage.models import *
import historypage.views

class HistoryPageUnitTest(TestCase):
    def test_transaksi_page(self):
        response = Client().get('/transaction/')
        self.assertEqual(response.status_code, 200)

    def test_string_formatting_transaksi_page(self) :
        kategori = Kategori.objects.create(nama="kategori_test")
        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000000,
            stok=1234,
            deskripsi="test_test",
            kategori=kategori,
        )
        kupon = Kupon.objects.create(
            nama_kupon="kupon_test",
            jenis_kupon="kupon_test",
            sk_kupon="apa",
            sk_singkat_kupon = "apa",
            kode="kode_test",
            persen_diskon=50,
            harga_minimum=1000,
            tanggal_expired_kupon=datetime.datetime.now()+datetime.timedelta(days=1)
        )
        
        Transaksi.objects.create(
            nama_pembeli="dummy",
            barang=barang,
            kupon=kupon,
            total_transaksi=1000000,
            tanggal_beli=datetime.datetime.now()
        )

        response = Client().get('/transaction/')
        html_response = response.content.decode('utf8')
        self.assertIn("1.000.000", html_response)
