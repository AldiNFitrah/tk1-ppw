from django.shortcuts import render
from .models import Transaksi
# Create your views here.
def index(request):
    transaksi = Transaksi.objects.all()
    for x in range(len(transaksi)):
        transaksi[x].total_transaksi = f'{transaksi[x].total_transaksi:,}'.replace(',','.')
    return render(request, 'history.html' , {'transaksi' : transaksi})
